"""
Program to generate XML data for Fritizing Part for Box0 upper board
"""

xml_format = """
<connector name="{name}" id="connector{id}" type="male">
	<description>{desc}</description>
	<views>
		<breadboardView>
			<p svgId="connector{id}pin" layer="breadboardbreadboard"/>
		</breadboardView>
		<schematicView>
			<p svgId="connector{id}pin" terminalId="connector{id}terminal" layer="schematic"/>
		</schematicView>
		<pcbView>
			<p svgId="connector{id}pin" layer="copper0"/>
			<p svgId="connector{id}pin" layer="copper1"/>
		</pcbView>
	</views>
</connector>
"""

data = [
	["SPI0.SS{i}", "SPI0 — Slave Select {i}", 4],
	["SPI0.SCLK", "SPI0 — Serial Clock", 1],
	["SPI0.MISO", "SPI0 — Master In, Slave Out", 1],
	["SPI0.MOSI", "SPI0 — Master Out, Slave In", 1],

	["PWM0.CH0", "PWM0 — Channel 0", 1],
	["PWM1.CH0", "PWM1 — Channel 0", 1],

	["UART0.TX", "UART0 — Transmission", 1],
	["UART0.RX", "UART0 — Reception", 1],

	["AOUT0.CH{i}", "AOUT0 — Channel {i}", 2],

	["AIN0.CH{i}", "AIN0 — Channel {i}", 4],

	["+5V", "+5V Power", 1],
	["-5V", "-5V Power", 1],

	["3V3", "+3.3V Power", 1],
	["GND", "Ground", 21],

	["REF", "Analog Reference", 1],

	["DIO0.{i}", "DIO0 — Pin {i}", 8],

	["I2C0.SDA", "I2C0 — Serial Data", 1],
	["I2C0.SCL", "I2C0 — Serial Clock", 1]
]

bus_list = {}

conn_id = 0
for name, desc, count in data:
	for i in range(count):
		xmlname = name.format(i=i)
		xmldesc = desc.format(i=i)
		xmlid = conn_id
		xml = xml_format.format(id=xmlid, desc=xmldesc, name=xmlname)

		print(xml)

		conn_id += 1

		# http://stackoverflow.com/a/473108/1500988
		if xmlname in bus_list:
			bus_list[xmlname].append(xmlid)
		else:
			bus_list[xmlname] = [xmlid]

bus_id = 1
for i in bus_list:
	if len(bus_list[i]) > 1:
		print("""<!-- {name} -->""".format(name=i))
		print("""<bus id="internal{id}">""".format(id=bus_id))
		for j in bus_list[i]:
			print("""\t<nodeMember connectorId="connector{id}"/>""".format(id=j))
		print("""</bus>""")
		print()
		bus_id += 1
